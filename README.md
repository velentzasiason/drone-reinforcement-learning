# Drone - Reinforcement Learning

The purpose of this project is to let a drone learn autonomously (employing Reinforcement Learning techniques) how to fly over a wall lying in front of it and reach a specific location without exceeding particular limits per axis.

## Simulation Setup

The current project is created using [Ubuntu 14.04](https://releases.ubuntu.com/14.04/) with [ROS Indigo](http://wiki.ros.org/indigo). The selected drone is [ArDrone 2.0](http://wiki.ros.org/ardrone_autonomy) quadrocopter and the environment is completed using the [TUM Simulator](http://wiki.ros.org/tum_simulator).

_Note 1: For more info about setting up the simulator with ardrone\_autonomy package see this [link](https://github.com/dougvk/tum_simulator#readme)_

_Note 2: In case you encounter problems loading the gazebo simulation it is suggested that you download the models from models.gazebosim.org and store them locally so that they can be loaded in the simulation. This is an easy workaround for the change of URI from gazebosim.org/models to models.gazebosim.org_

## Problem Description

### Environment Modelling

The main element of the environment is the wall lying in front of the robot. The robot is initially landed at the origin of the cartesian framework (0,0,0). The wall is a cuboid with dimensions of (1 x 4 x 2) and its center is located at (1.75, 0, 1). 

We also consider a cuboid in which the drone is allowed to move. Each dimension is defined:

-- x: (-0.50, +4.00)

-- y: (-2.25, +2.25)

-- z: ( 0.00, +3.00)

The above mentioned cuboid will be discretized in cells creating a (3x3x3) grid with equal cuboid cells. 

<p align=" center ">
  <img src="/media/grid.png" width="24%" />
</p>

### States

Each Cell of the grid is a state the drone can be in as well as an extra one modelling the exit of the grid. So, State 0 is when the robot tries to exit the grid. Then states 1 through 27 model the cells of the grid. The enumeration of the cells can be seen in the following image:


As we can see the START_STATE is state-4, the GOAL_STATE is state-3 and the HOLE_STATES apart from state-0 are: (2,5,8,11,14,17) which are the states containing the wall and model the drone's crush in the wall.

<p float="left">
  <img src="/media/2d_1.jpg" width="20%" />
  <img src="/media/2d_2.jpg" width="20%" /> 
  <img src="/media/2d_3.jpg" width="20%" />
</p>


### Actions

We assume that the drone can move only in a translational way (no rotations are allowed) and only in one axis per time. So, the actions that can use the robot are:

[-Z, -Y, -X, +X, +Y, +Z] == [down, left, backwards, forward, right, up]

### Learning

For the learning part a Q-learning and a SARSA algorithms are implemented. 

Initially, we employ the following set of rewards: R = (-1, +1, 0) for achieving Hole/Wall State, Goal State and anything else, respectively.

There are a couple of problems observed, while training the drone (even for a longer number of episodes):

    -- The drone sometimes iterates back and forth falling in an infinite loop
    -- The drone frequently finds sub-optimal solutions with longer than 7 steps (which is the optimal).

To handle these problems we decided to modify the rewards in order to punish the robot for staying longer in the maze. This is done by a new set of rewards, namely: R = (-1, +1, -1e-6).


### Drone Movement

The drone uses a PID Controller in order to move between the cells. The goal in each movement is for the drone (its CoM) to approach the center of the (next) target cell with zero final velocity in each axis. Since there is no need for precision during these movements, for the sake of simplicity the I-part will be omitted.

An example of the movement can be seen here (8x speed):

<p align=" center ">
  <img src="/media/Drone_Demo.gif" width="75%" />
</p>


## Results

For each table the results are drawn from a set of 1000 trainings.

SARSA Algorithm with initial Rewards

| Number of Episodes      | 100 | 200     | 300 | 500|
| :---        |    :----:   |          :----: |:----: | ---: |
| % of Successful Paths  | 52.6 |  98.8  | 100  | 99.9 |
| % of Optimal Paths   |      41.6   | 63.5 | 66.5 | 74.6|
| % of Near-Optimal Paths   | 83.5  | 97.2 | 99.9 |  99.3 |

Q-learning Algorithm with initial Rewards

| Number of Episodes      | 100 | 200     | 300 | 500|
| :---        |    :----:   |          :----: |:----: | ---: |
| % of Successful Paths  | 49.6 |  99.8  | 100  | 100 |
| % of Optimal Paths   |      44.2   | 84.5 | 83.0 | 84.1|
| % of Near-Optimal Paths   | 51.2  | 99.4 | 99.5 |  99.8 |


SARSA Algorithm with modified Rewards

| Number of Episodes      | 100 | 200     | 300 | 500|
| :---        |    :----:   |          :----: |:----: | ---: |
| % of Successful Paths  | 48.6 |  98.6  | 100  | 99.9 |
| % of Optimal Paths   |      45.3   | 65.2 | 67.9 | 77.3|
| % of Near-Optimal Paths   | 92.6  | 97.9 | 98.2 |  99.7 |

Q-learning Algorithm with modified Rewards

| Number of Episodes      | 100 | 200     | 300 | 500|
| :---        |    :----:   |          :----: |:----: | ---: |
| % of Successful Paths  | 85.9 |  100  | 100  | 100 |
| % of Optimal Paths   |      96.5   | 99.9 | 99.9 | 99.9|
| % of Near-Optimal Paths   | 99.7  |100 |100 | 100 |


