# CMake generated Testfile for 
# Source directory: /home/lightlord142/tum_simulator_ws/src
# Build directory: /home/lightlord142/tum_simulator_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
SUBDIRS(gtest)
SUBDIRS(tum_simulator/cvg_sim_msgs)
SUBDIRS(tum_simulator/tum_simulator)
SUBDIRS(drone_application)
SUBDIRS(ardrone_autonomy)
SUBDIRS(tum_simulator/cvg_sim_gazebo)
SUBDIRS(tum_simulator/cvg_sim_gazebo_plugins)
SUBDIRS(tum_simulator/message_to_tf)
