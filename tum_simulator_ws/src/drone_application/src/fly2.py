#! /usr/bin/env python

import rospy
import time
import random
import numpy as np
import math
import pandas as pd
import csv
from std_msgs.msg import Empty
from geometry_msgs.msg import Twist, Pose
from std_msgs.msg import String
from ardrone_autonomy.msg import Navdata
from gazebo_msgs.srv import GetLinkState, GetLinkStateRequest
from simulation import Simulation
import autonomousflight as af
import learning_mod as lrn




if __name__ == '__main__':

	try:
		sim = Simulation()
		sim.pause()
		num_of_steps = {}
		ratio_of_succ = {0: 0, 1: 0}
		eps = 0.9
		gamma = 0.9
		alpha = 0.1
		n_ep = 300
		for i in range(1000):
			print(i)
			q_table = lrn.learningSARSA(eps, alpha, n_ep, gamma)
			#q_table = pd.read_csv('q_table%d%.1f_%.2f%.2f.csv' %(n_ep, eps, a, gamma ))
			trajectory, res = lrn.motion_application(q_table)
			#print(trajectory)
			ratio_of_succ[res] += 1
			if res==0 :
				if len(trajectory) in num_of_steps:
					num_of_steps[len(trajectory)] += 1
				else:
					num_of_steps[len(trajectory)] = 1
		print(n_ep)
		print(num_of_steps)
		print(ratio_of_succ)
		a_file = open("res_mod_3_SAR.csv", "w")
		writer = csv.writer(a_file)
		for key, value in num_of_steps.items():
			writer.writerow([key, value])
		a_file.close()

	except rospy.ROSInterruptException:
		pass



