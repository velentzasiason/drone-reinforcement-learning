#! /usr/bin/env python

import random
import numpy as np
import math
import pandas as pd
import autonomousflight as af


# Maze Modelling Holes, Goals
start =(0,1,0)
START_STATE = 4
goal = (2,2,0)
GOAL_STATE = 9
holes = [[1,0,0],[1,1,0],[1,2,0],[1,0,1],[1,1,1],[1,2,1]]
HOLE_STATES = (0,2,5,8,11,14,17)
#is_TERMINAL_STATE = False

# Learning Parameters
N_EPISODES_MAX = 1
gamma = 0.9
epsilon = 0.9
alpha = 0.1 





def find_action(observed_state, q_table, eps):

	if np.random.uniform() < eps:

		# Find Q of actions for this state from Q_table:
		action_state = q_table.loc[observed_state,:]
		selected_action = np.random.choice(action_state[action_state == np.max(action_state)].index)
	else:
		selected_action = np.random.choice(af.ACTIONS)

	return selected_action


def environment_feedback(state, action):

	# ACTIONS = ['-Z', '-Y', '-X', '+X', '+Y', '+Z']
	# action := [ -3,   -2,   -1,   +1,   +2,   +3 ]
	new_state = 0
	reward = -0.00001
	is_TERMINAL_STATE = False
	[i, j, k] = af.state2cell(state)
	#print([i,j,k])
	if action == '-Z':
		k -= 1
		#new_state = state - M*N
	elif action == '+Z':
		if k < af.Q-1:
			k += 1
		else:
			k = -1
		#new_state = state + M*N
	elif action == '-Y':
		j -= 1
		#new_state = state - 1
	elif action == '+Y':
		if j< af.N-1:
			j += 1
		else:
			j = -1
		#new_state = state + 1
	elif action == '-X':
		i -= 1
		#new_state = state - N
	elif action == '+X':
		if i < af.M-1:
			i += 1
		else:
			i = -1
		#new_state = state + N
	new_state = af.cell2state(i,j,k)
	#print(new_state)
	if new_state not in range(1,28):
		new_state = 0
		reward = -1.0

	if  new_state in HOLE_STATES:
		reward = -1.0
		is_TERMINAL_STATE = True

	if new_state == GOAL_STATE:
		reward = +1.0
		is_TERMINAL_STATE = True

	return new_state, reward, is_TERMINAL_STATE



def learningQ(e, a, n_ep, g):

	q_table = pd.DataFrame(np.zeros((af.M*af.N*af.Q+1, len(af.ACTIONS))), columns = af.ACTIONS)
	episode = 0
	cumulative_reward = []

	while episode < n_ep:
		is_TERMINAL_STATE = False
		state = START_STATE
		step = 0
		cum_rew = 0
		#print(episode)
		while not is_TERMINAL_STATE:
			
			action = find_action(state, q_table, e)
			
			new_state, reward, is_TERMINAL_STATE = environment_feedback(state, action)	
			q_pr = q_table.loc[state, action]

			if not is_TERMINAL_STATE:
				q_t = reward + g *q_table.iloc[new_state].max()
			else:
				q_t = reward

			q_table.loc[state, action] += a *(q_t-q_pr)
			cum_rew += reward
			state = new_state
			step += 1
			if step >= 50:
				is_TERMINAL_STATE = True
		cumulative_reward.append(cum_rew)
		episode += 1
	#q_table.to_csv('q_table%d%.1f_%.2f%.2f.csv' %(n_ep, e, a, g ), index = False)

	return q_table#, cumulative_reward


def learningSARSA(e, a, n_ep, g):

	q_table = pd.DataFrame(np.zeros((af.M*af.N*af.Q+1, len(af.ACTIONS))), columns = af.ACTIONS)
	episode = 0
	cumulative_reward=[]
	while episode < n_ep:
		is_TERMINAL_STATE = False
		state = START_STATE
		step = 0
		action = find_action(state, q_table, e)
		cum_rew = 0
		#print(episode)
		while not is_TERMINAL_STATE:
			
			
			new_state, reward, is_TERMINAL_STATE = environment_feedback(state, action)	
			new_action = find_action(new_state, q_table, e)

			q_pr = q_table.loc[state, action]

			if not is_TERMINAL_STATE:
				q_t = reward + g *q_table.loc[new_state, new_action]
			else:
				q_t = reward

			cum_rew += reward
			q_table.loc[state, action] += a *(q_t-q_pr)

			action = new_action
			state = new_state
			step += 1
			if step >= 50:
				is_TERMINAL_STATE = True
		cumulative_reward.append(cum_rew)
		episode += 1
	#q_table.to_csv('q_table%d%.1f_%.2f%.2f.csv' %(n_ep, e, a, g ), index = False)

	return q_table#, cumulative_reward

def motion_application(q_table):


	trajectory = []
	global START_STATE
	global GOAL_STATE
	global HOLE_STATES

	state = START_STATE
	step = 0
	is_TERMINAL_STATE = False
	while not is_TERMINAL_STATE:

		action_state = q_table.loc[state,:]
		action = np.random.choice(action_state[action_state == np.max(action_state)].index)
		#print("Step: "+str(step))
		#print("From state: " + str(state) + " selected action: " + str(action) )
		trajectory.append(action)
		new_state, reward, is_TERMINAL_STATE = environment_feedback(state, action)	
		state = new_state
		step += 1
		if step >= 50:
				is_TERMINAL_STATE = True

	if state == GOAL_STATE:
		#print("Success")
		res = 0
	else:
		#print("Failure")
		res = 1

	return trajectory, res
