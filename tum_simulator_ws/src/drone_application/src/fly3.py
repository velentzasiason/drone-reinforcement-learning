#! /usr/bin/env python

import rospy
import time
import random
import numpy as np
import math
import pandas as pd

from std_msgs.msg import Empty
from geometry_msgs.msg import Twist, Pose
from std_msgs.msg import String
from ardrone_autonomy.msg import Navdata
from gazebo_msgs.srv import GetLinkState, GetLinkStateRequest
from simulation import Simulation

FACTOR = 0.1
V_LIN = [[0.0,0.0,0.0], [1.0,0.0,0.0], [0.0,1.0,0.0], [0.0,0.0,1.0]]
DIR = ['down', 'right', 'backwards', 'nowhere', 'forward', 'left', 'up']
ACTIONS = ['-Z', '-Y', '-X', '+X', '+Y', '+Z']

# Maze Modelling (Slicing into cells)
Xmin = -0.5
Xmax = 4.0
Ymin = -2.25
Ymax = +2.25
Zmin = 0.0
Zmax = 3.0
M = 3
N = 3
Q = 3
Xden = (Xmax - Xmin)/M
Yden = (Ymax - Ymin)/N
Zden = (Zmax - Zmin)/Q
Xcenters = []
Ycenters = []
Zcenters = []

for k in range(0,M):
	Xcenters.append(Xmin + (2*k+1)*Xden/2)
for k in range(0,N):
	Ycenters.append(Ymin + (2*k+1)*Yden/2)
for k in range(0,Q):
	Zcenters.append(Zmin + (2*k+1)*Zden/2)


# Maze Modelling Holes, Goals
start =(0,1,0)
START_STATE = None
goal = (2,1,0)
GOAL_STATE = None
holes = [[1,0,0],[1,1,0],[1,2,0],[1,0,1],[1,1,1],[1,2,1]]
HOLE_STATES = None
is_HOLE_STATE = False

# Learning Parameters
N_EPISODES_MAX = 100
gamma = 0.9
epsilon = 0.9


# + 1 state surrounding the maze which is HOLE (BLACK HOLE)

class PD_Controller():

	def __init__(self):
		self.Kp = 0.01
		self.Kd = 0.001


	def move(p1, p2, v_curr):
		v =  []
		v.append(Kp*(p2[0]-p1[0])-Kd*v_curr[0])
		v.append(Kp*(p2[1]-p1[1])-Kd*v_curr[1])
		v.append(Kp*(p2[2]-p1[2])-Kd*v_curr[2])
		return v

class AutonomousFlight():

	def __init__(self):

		rospy.init_node('fly3', anonymous = False)
		self.rate = rospy.Rate(3)
		self.takeoff_pub = rospy.Publisher('/ardrone/takeoff', Empty, queue_size = 1)
		self.land_pub = rospy.Publisher('/ardrone/land', Empty, queue_size=1)
		self.reset_pub = rospy.Publisher('ardrone/reset', Empty, queue_size=1)
		self.vel_pub = rospy.Publisher('/cmd_vel', Twist, queue_size=1)
		self.navData_sub = rospy.Subscriber('/ardrone/navdata',Navdata,self.callback)
		#self.sonar_sub = rospy.Subscriber('/sonar_height',TFMessage,self.callback_geom)

		self.vel = Twist()
		self.navdata_state = -1
		self.navdata_altitude = 0
		rospy.wait_for_service('/gazebo/get_link_state')
		self.link_state_srv = rospy.ServiceProxy('/gazebo/get_link_state', GetLinkState)
		self.link = GetLinkStateRequest()
		self.link_name = 'quadrotor::base_link'
		self.pose = Pose()
		self.x = 0
		self.y = 0
		self.z = 0
		[self.i, self.j, self.k] = coos2cell(self.x, self.y, self.z)
		


	def update_robot_pose(self):
		res = self.link_state_srv(self.link_name, None)
		self.pose = res.link_state.pose
		self.x = self.pose.position.x
		self.y = self.pose.position.y
		self.z = self.pose.position.z

	def callback(self, data):
		self.navdata_state = data.state
		self.navdata_altitude = data.altd
		self.vx = data.vx
		self.vy = data.vy
		self.vz = data.vz

	def callback_geom(self,data):
		rospy.loginfo("dir x"+str(data.transform.translation.x))

	def TakingOff(self):
		#if self.navdata_state == 2:
		self.takeoff_pub.publish(Empty())
		rospy.loginfo('Starting the trip')

	def Landing(self):
		self.land_pub.publish(Empty())
		rospy.loginfo('Landing in London')

	def Resetting(self):
		self.reset_pub.publish(Empty())
		rospy.loginfo('Resetting drone')


	def Move(self, motion):
		# motion takes values: [-3,3]
		# 0 : stop
		# 1 : x_axis - forward, -1: x_axis -backwards
		# 2 : y_axis - left, -2: y_axis - right
		# 3 : z_axis - up, -3: z_axis - down
		
		ind = abs(motion)
		if ind > 0:
			sign = motion/ind
		else:
			sign = 0.0
		self.vel.linear.x = sign*V_LIN[ind][0]*FACTOR
		self.vel.linear.y = sign*V_LIN[ind][1]*FACTOR
		self.vel.linear.z = sign*V_LIN[ind][2]*FACTOR
		self.vel.angular.x = 0.0
		self.vel.angular.y = 0.0
		self.vel.angular.z = 0.0
		self.vel_pub.publish(self.vel)
		rospy.loginfo('Moving ' + DIR[motion+3])
"""
	def moveCell(self, motion):
		global Xcenters
		global Ycenters
		global Zcenters
		p1 = [Xcenters[i], Ycenters[j], Zcenters[k]]
		ind = abs(motion)
		if ind>0:
			sign = motion/ind
		else:
			sign = 0.0
		p2 = [Xcenters[i+int(V_LIN[ind][0])], Ycenters[j+int(V_LIN[ind][1])], Zcenters[k+int(V_LIN[ind][2])]
"""		
		
def coos2cell(x,y,z):

	if x>=Xmin-MARGIN and x<=Xmax+MARGIN:
		i = (x-Xmin)//Xden
	else:
		return [-1,-1,-1]

	if y>=Ymin-MARGIN and y<=Ymax+MARGIN:
		j = (y-Ymin)//Yden
	else:
		return [-1,-1,-1]

	if z>=Zmin-MARGIN and z<=Zmax+MARGIN:
		k = (z-Zmin)//Zden
	else:
		return [-1,-1,-1]

	return [i,j,k] 


def cell2state(i,j,k):
	if i+j+k>=0:
		return i*N + j + k*M*N + 1
	else:
		# Zero State for outside grid and 1-27 for the other
		return 0

def find_next_point(x, y, z, motion):
	g


def find_action(observed_state, q_table):

	if np.random.uniform() < epsilon:

		# Find Q of actions for this state from Q_table:
		action_state = q_table.loc[observed_state,:]
		selected_action = np.random.choice(action_state[action_state == np.max(action_state)].index)
	else:
		selected_action = np.random.choice(ACTIONS)

	return selected_action


def environment_feedback(state, action):

	# HERE IT BECOMES THE MODELLING
	new_state = 0
	reward = 0
	is_HOLE_STATE = True


	return new_state, reward, is_HOLE_STATE


def setup_environment(start, goal, holes):

	global START_STATE
	global GOAL_STATE
	global HOLE_STATES
	START_STATE = coos2cell(start[0],start[1],start[2])
	GOAL_STATE = coos2cell(goal[0],goal[1],goal[2])
	hole_list = [0]
	for k in holes:
		hole_list.append(coos2cell(k[0],k[1],k[2])) 
	HOLE_STATES = tuple(hole_list)


def learning():

	global start
	global holes
	global goal
	q_table = pd.DataFrame(np.zeros((M*N*Q), len(ACTIONS)), columns = ACTIONS)
	episode = 0
	while episode < N_EPISODES_MAX:
		setup_environment(start, goal, holes)
		is_HOLE_STATE = False
		state = START_STATE
		step = 0
		# I don't think the environment needs update for now
		#update_environment(state, episode, step)
		while not is_HOLE_STATE:

			action = find_action(state, q_table)

			new_state, reward, is_HOLE_STATE = environment_feedback(state, action)

			if new_state != GOAL_STATE:
				q_t = reward + gamma #* SOMETHING!!!!!!!
			else:
				q_t = reward

			#SOMETHING WITH q_table
			state = new_state
			step += 1
			# I don't think the environment needs update for now
			#update_environment(state, episode, step)
		episode += 1
	return q_table


if __name__ == '__main__':

	try:
		ctr = 0
		sim = Simulation()
		drone = AutonomousFlight()
		ctrl = PD_Controller()
		while drone.navdata_state not in [3,6,7]:
			drone.TakingOff()
			drone.rate.sleep()

		p2 = [0.0,  0.0, 2.0]
		while not rospy.is_shutdown():
			print("[" + str(ctr) + "]")
			drone.update_robot_pose()
			#print(drone.navdata_altitude)
			[drone.i, drone.j, drone.k] = coos2cell(drone.x, drone.y, drone.z)
			r = cell2state(drone.i,drone.j,drone.k)

			print('x = ' + str(drone.x) + 'y = ' + str(drone.y)+'z = ' + str(drone.z)+ '\n')
			print('i = ' + str(drone.i)+ ', j = '+ str(drone.j) + ', k ='+ str(drone.k))
			print('State = ' +str(r))
			p1 = [drone.x, drone.y, drone.z]
			vcurr = [drone.vx, drone.vy, drone.vz]
			v = ctrl.move(p1, p2, vcurr )
			drone.vel.linear.x = vel[0]
			drone.vel.linear.y = vel[1]
			drone.vel.linear.z = vel[2]
			drone.vel.angular.x = 0.0
			drone.vel.angular.y = 0.0
			drone.vel.angular.z = 0.0
			drone.vel_pub.publish(self.vel)
			rospy.loginfo('Moving ' + DIR[motion+3])
			"""
			if ctr <=20:
				drone.Move(3)
				if ctr == 10:
					sim.pause()
					time.sleep(5)
					sim.unpause()
			else:
				break
			"""
			drone.rate.sleep()
			time.sleep(1)
			ctr += 1

		drone.Landing()

		while drone.navdata_state==8:
			drone.rate.sleep(1)
		sim.reset()
		drone.Resetting()

	except rospy.ROSInterruptException:
		pass



