#! /usr/bin/env python

import rospy

from std_srvs.srv import Empty

class Simulation():

	def __init__(self):

		self.pause = rospy.ServiceProxy('/gazebo/pause_physics', Empty)
		self.unpause = rospy.ServiceProxy('/gazebo/unpause_physics', Empty)
		self.reset = rospy.ServiceProxy('/gazebo/reset_simulation', Empty)


	def pause():
		rospy.wait_for_service('/gazebo/pause_physics')
		try: 
			self.pause()
			print("Pausing Simulation Successful")
		except rospy.ServiceException, e:
			print("Pausing Simulation Failed")

	def unpause():
		rospy.wait_for_service('/gazebo/unpause_physics')
		try: 
			self.unpause()
			print("Unpausing Simulation Successful")
		except rospy.ServiceException, e:
			print("Unpausing Simulation Failed")

	def reset():
		rospy.wait_for_service('/gazebo/reset_simulation')
		try: 
			self.reset()
			print("Resetting Simulation Successful")
		except rospy.ServiceException, e:
			print("Resetting Simulation Failed")