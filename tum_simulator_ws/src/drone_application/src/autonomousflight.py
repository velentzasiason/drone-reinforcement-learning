#! /usr/bin/env python

import rospy
import time
import random
import numpy as np
import math
import pandas as pd
import sys

from std_msgs.msg import Empty
from geometry_msgs.msg import Twist, Pose
from std_msgs.msg import String
from ardrone_autonomy.msg import Navdata
from gazebo_msgs.srv import GetLinkState, GetLinkStateRequest



global Xmin
global Xmax
global Ymin
global Ymax
global Zmin
global Zmax
global M
global N
global Q
global Xden
global Yden
global Zden
global MARGIN

# Maze Modelling (Slicing into cells)
Xmin = -0.5
Xmax = 4.0
Ymin = -2.25
Ymax = +2.25
Zmin = 0.0
Zmax = 3.0
M = 3
N = 3
Q = 3
Xden = (Xmax - Xmin)/M
Yden = (Ymax - Ymin)/N
Zden = (Zmax - Zmin)/Q
MARGIN = 0.05

# Movements Modelling
V_LIN = [[0.0,0.0,0.0], [1.0,0.0,0.0], [0.0,1.0,0.0], [0.0,0.0,1.0]]
DIR = ['down', 'right', 'backwards', 'nowhere', 'forward', 'left', 'up']
ACTIONS = ['-Z', '-Y', '-X', '+X', '+Y', '+Z']
MOTIONS_INDEX = [-3, -2, -1, 1, 2, 3]



def cell2state(i,j,k):

	if min([i,j,k])>=0:
		return i + j*N + k*M*N + 1
	else:
		# Zero State for outside grid and 1-27 for the other
		return 0


def state2cell(s):


	if s==0:
		return [-1, -1, -1]
	else:
		k = (s - 1)//(M*N)
		j = (s - k*M*N - 1)//N
		i = s - k*M*N - j*N - 1
		return [i, j, k]

def find_next_point(x, y, z, motion):


	k_x = (x - Xmin)//Xden
	k_y = (y - Ymin)//Yden
	k_z = (z - Zmin)//Zden

	x_c = Xmin + (k_x + 0.5)*Xden
	y_c = Ymin + (k_y + 0.5)*Yden
	z_c = Zmin + (k_z + 0.5)*Zden

	ind = abs(motion)
	if ind > 0:
		sign = motion/ind
	else:
		sign = 0.0

	x_c += V_LIN[ind][0]*Xden*sign
	y_c += V_LIN[ind][1]*Yden*sign
	z_c += V_LIN[ind][2]*Zden*sign

	return [x_c, y_c, z_c]


def coos2cell(x,y,z):

	if x>=Xmin-MARGIN and x<=Xmax+MARGIN:
		i = (x-Xmin)//Xden
	else:
		return [-1,-1,-1]

	if y>=Ymin-MARGIN and y<=Ymax+MARGIN:
		j = (y-Ymin)//Yden
	else:
		return [-1,-1,-1]

	if z>=Zmin-MARGIN and z<=Zmax+MARGIN:
		k = (z-Zmin)//Zden
	else:
		return [-1,-1,-1]

	return [i,j,k] 



class AutonomousFlight():

	def __init__(self):

		rospy.init_node('fly2', anonymous = False)
		self.rate = rospy.Rate(3)
		self.takeoff_pub = rospy.Publisher('/ardrone/takeoff', Empty, queue_size = 1)
		self.land_pub = rospy.Publisher('/ardrone/land', Empty, queue_size=1)
		self.reset_pub = rospy.Publisher('ardrone/reset', Empty, queue_size=1)
		self.vel_pub = rospy.Publisher('/cmd_vel', Twist, queue_size=1)
		self.navData_sub = rospy.Subscriber('/ardrone/navdata',Navdata,self.callback)
		#self.sonar_sub = rospy.Subscriber('/sonar_height',TFMessage,self.callback_geom)

		self.vel = Twist()
		self.navdata_state = -1
		self.navdata_altitude = 0
		rospy.wait_for_service('/gazebo/get_link_state')
		self.link_state_srv = rospy.ServiceProxy('/gazebo/get_link_state', GetLinkState)
		self.link = GetLinkStateRequest()
		self.link_name = 'quadrotor::base_link'
		self.pose = Pose()
		self.x = 0
		self.y = 0
		self.z = 0
		[self.i, self.j, self.k] = coos2cell(self.x, self.y, self.z)
		
		self.Kp = 0.7
		self.Kd = 0.002

	def update_robot_pose(self):
		res = self.link_state_srv(self.link_name, None)
		self.pose = res.link_state.pose
		self.x = self.pose.position.x
		self.y = self.pose.position.y
		self.z = self.pose.position.z
		#rospy.loginfo("Current Position: " + str(self.pose.position))

	def callback(self, data):
		self.navdata_state = data.state
		self.navdata_altitude = data.altd
		self.vx = data.vx
		self.vy = data.vy
		self.vz = data.vz


	def TakingOff(self):
		#if self.navdata_state == 2:
		self.takeoff_pub.publish(Empty())
		rospy.loginfo('Starting the trip')

	def Landing(self):
		self.land_pub.publish(Empty())
		rospy.loginfo('Landing in London')

	def Resetting(self):
		self.reset_pub.publish(Empty())
		rospy.loginfo('Resetting drone')
	
	def pd_control(self, p1, p2, v_curr):
		self.vel.linear.x = (self.Kp*(p2[0]-p1[0])-self.Kd*v_curr[0])
		self.vel.linear.y = (self.Kp*(p2[1]-p1[1])-self.Kd*v_curr[1])
		self.vel.linear.z = (self.Kp*(p2[2]-p1[2])-self.Kd*v_curr[2])
		self.vel.angular.x = 0.0
		self.vel.angular.y = 0.0
		self.vel.angular.z = 0.0
		self.vel_pub.publish(self.vel)
		#rospy.loginfo('Published Velocity: ' + str(self.vel.linear))
		#rospy.loginfo(self.vel.linear)

	def move_with_pd(self, movement):
		
		global ACTIONS
		global MOTIONS_INDEX
		ind = ACTIONS.index(movement)
		self.update_robot_pose()
		p1 = [self.x, self.y, self.z]
		p2 = find_next_point(self.x, self.y, self.z, MOTIONS_INDEX[ind])	
		error = max([abs(p1[0]-p2[0]), abs(p1[1]-p2[1]), abs(p1[2]-p2[2])])
		print(movement)
		while error>=0.1:
			print(error)
			self.update_robot_pose()
			p1 = [self.x, self.y, self.z]
			v_curr = [self.vx, self.vy, self.vz]
			self.pd_control(p1,p2,v_curr)
			self.rate.sleep()
			error = max([abs(p1[0]-p2[0]), abs(p1[1]-p2[1]), abs(p1[2]-p2[2])])
